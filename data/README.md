# WARNING
Do not use BIN 100 for testing / training / validation.
That data is present in this folder, as `sample data` with the correct tags.
Failure to heed this warning could lead to skewed results.