import os
import pathlib
import requests


def upload_image(path):
    url = "http://localhost:5000/images"
    files = {'image': open(path, 'rb')}
    r = requests.post(url, files=files)


if __name__ == "__main__":
    rel_parent = pathlib.Path(__file__).parent
    for(dirpath, dirnames, filenames) in os.walk(rel_parent/"images"):
        for file in filenames:
            upload_image(rel_parent/dirpath/file)
