from node:16

WORKDIR /usr/src/frontend
COPY ./frontend ./

RUN npm install && npm run build

WORKDIR /usr/src/backend
copy ./node-server ./
RUN npm install && cp -r ../frontend/build/ build

CMD ["npm", "run", "start"]
