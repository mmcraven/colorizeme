# ColorizeMe

Colorize B&W images using ML!

## Install

- Install NVM on host machine. [Directions are here.](https://github.com/nvm-sh/nvm)
- Install node:16 using `nvm install 16`.
- Install docker on host machine. [Directions are here.](https://docs.docker.com/engine/install/ubuntu/)
- Install /requirements.txt into you system python. Or, create a virtualenv, and modify the .vscode settings to do lint correctly.
- Profit.

cd to frontend and `npm i`.
cd to node-server and `npm i`.
This will give autocomplete inside the ide.

## Making containers work

- `docker compose -f dev.yaml -p dev up --build` to start.
- `docker bompose -f dev.yaml -p dev down` to turn off.
