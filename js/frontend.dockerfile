FROM node:16

WORKDIR /usr/src/
COPY ./ ./
WORKDIR /usr/src/frontend-web
RUN npm install

CMD ["npm", "start"]