import React from 'react';
import { render, screen } from '@testing-library/react';
import 'jest';
import '@testing-library/jest-dom/extend-expect';
import App from './App';

test('renders title in documet', () => {
  render(<App />);
  const aboutElements = screen.getAllByText(/About/i);
  aboutElements.forEach((element) => expect(element).toBeInTheDocument());
});
