import React from 'react';
import { RecoilRoot } from 'recoil';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';

import {
  Footer, Header, BodyAbout, BodyUpload, BodyView,
} from '@mmcraven/frontend-component-library';

const App = (): React.ReactElement => (
  <div className="App">
    <RecoilRoot>
      {/* See router docs https://reactrouter.com/docs/en/v6/faq */}
      <Router>
        <div className="footer-wrapper">
          <Header />
          <Routes>
            <Route path='/' element={<BodyAbout />} />
            <Route path='/about' element={<BodyAbout />} />
            <Route path="/view" element={<BodyView />} />
            <Route path="/upload" element={<BodyUpload />} />
          </Routes>
          <Footer />
        </div>
      </Router>
    </RecoilRoot>
  </div>
);

export default App;
