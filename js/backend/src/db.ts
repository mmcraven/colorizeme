import * as mongodb from 'mongodb';

const connectionString = 'mongodb://root:example@mongo:27017/?retryWrites=true&w=majority';

const getDBConnection = async () => {
  const client = new mongodb.MongoClient(connectionString);
  return client.connect();
};

export const getDB = async (): Promise<[mongodb.MongoClient,
  mongodb.Collection<mongodb.Document>]> => {
  const conn = await getDBConnection();
  const imageDB = conn.db('images');
  return [conn, imageDB.collection('default_user')];
};

export const imageTable = async (): Promise<[mongodb.MongoClient,
  mongodb.Collection<mongodb.Document>]> => {
  const conn = await getDBConnection();
  const imp = conn.db('im_ptr');
  return [conn, imp.collection('default_user')];
};

export const listDatabases = async (client: mongodb.MongoClient) => {
  // eslint-disable-next-line no-console
  console.log(client);
  const databasesList = await client.db().admin().listDatabases();

  // eslint-disable-next-line no-console
  console.log('Databases:');
  // eslint-disable-next-line no-console
  databasesList.databases.forEach((db) => console.log(` - ${db.name}`));
};
