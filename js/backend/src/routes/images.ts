import * as express from 'express';
import * as multer from 'multer';
import * as imagesController from '../controllers/images';

const imagesRoutes = express.Router();

const upload = multer.default({ dest: '/tmp/uploads' });

imagesRoutes.get('/', imagesController.getImagesList);
imagesRoutes.post('/', upload.single('image'), imagesController.postImage);
// WARNING: Should be only used for testing purposes
imagesRoutes.delete('/', imagesController.deleteAllImages);
// END WARNING
imagesRoutes.get('/:imgidx', imagesController.getImageData);
imagesRoutes.patch('/:imgidx', imagesController.patchImage);
imagesRoutes.delete('/:imgidx', imagesController.deleteImageByHash);

export default imagesRoutes;
