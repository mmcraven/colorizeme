import express from 'express';
import getImage from '../controllers/imp';

const impRoutes = express.Router();

impRoutes.get('/:imghash', getImage);

export default impRoutes;
