import cors from 'cors';
import express from 'express';
import http from 'http';
import dotenv from 'dotenv';

import imageRouter from './routes/images';
import impRouter from './routes/imp';

dotenv.config({ path: './config.env' });

const port = 5000;
const app = express();

app.use(cors());
app.use(express.json());

// Demo endpoint that forwards request to Python backend,
app.get('/leet', (req, res) => {
  // Notice that we can invoke it via service name!
  http.get('http://py-server:5001/', (innerRes) => {
    const bodyChunks: any = [];
    innerRes.on('data', (chunk) => bodyChunks.push(chunk));
    innerRes.on('end', () => res.send(Buffer.concat(bodyChunks).toString()));
  });
});

app.use('/images', imageRouter);
app.use('/imp', impRouter);

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is running on port: ${port}`);
});
