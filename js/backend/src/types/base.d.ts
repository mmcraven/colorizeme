import Express from 'express';

interface BaseResponse extends Express.Response {
  status: (number: number) => BaseResponse;
  contentType: (contentType: string) => BaseResponse;
  send: (content: any) => BaseResponse;
  end: () => void
}
