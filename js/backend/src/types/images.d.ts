import Express from 'express';
import type { File } from 'multer';

import type { BaseResponse } from './base';

export interface PHashable {
  phash: string
  chash: string
}
export interface PtrMetadata extends PHashable {
  url: string
}
export interface ImageMetadata {
  index: number
  images: Array<PtrMetadata>
}

export interface ImageStats extends PHashable {
  width: number
  height: number
}

export interface ColorizedImage {
  colorized: string;
  stats: ImageStats
}

export interface UploadRequest extends Express.Request {
  file?: File | undefined
}
export interface UploadResponse extends BaseResponse {
}

export interface IndexRequest extends Express.Request {
  params: { imgidx: string }
}
export interface IndexResponse extends BaseResponse {
}
