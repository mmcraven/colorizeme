import Express from 'express';
import type { BaseResponse } from './base';

export interface ImgPtrRequest extends Express.Request {
  params: { imghash: string }
}
export interface ImgPtrResponse extends BaseResponse {
}
