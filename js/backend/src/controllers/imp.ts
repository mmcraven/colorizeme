import type { ImgPtrRequest, ImgPtrResponse } from '../types/imp';
import { imageTable } from '../db';

const getImage = async (req: ImgPtrRequest, res: ImgPtrResponse) => {
  const [conn, imgPtrCol] = await imageTable();
  const result = await imgPtrCol.findOne({ phash: req.params.imghash });
  if (!result) {
    res.status(404).send('Not found');
  } else {
    const image = Buffer.from(result.image.value(), 'binary');
    res.contentType('image/jpeg');
    res.send(image);
  }
  await conn.close();
  return res.end();
};

export default getImage;
