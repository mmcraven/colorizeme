import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';
import jimp from 'jimp';
import * as mongodb from 'mongodb';
import {
  IndexRequest, IndexResponse, UploadRequest, UploadResponse,
} from '../types/images';
import { BaseResponse } from '../types/base';
import type { ColorizedImage, ImageMetadata, ImageStats } from '../types/images';
import * as db from '../db';

const template = (hash: string) => `http://localhost:5000/imp/${hash}`;

async function enumerateImageMetadata() {
  const [conn, userImages] = await db.getDB();
  const metadata: Array<ImageMetadata> = [];
  function extract(image: mongodb.Document) {
    return { phash: image.phash, chash: image.chash, url: image.url ?? template(image.phash) };
  }
  const asArray = await userImages.find().toArray();
  asArray.forEach((doc) => {
    const images = [extract(doc.bw), extract(doc.color)];
    metadata.push({ index: doc.index, images });
  });
  await conn.close();
  return metadata;
}

async function retrieveImage(phash: string) {
  const [conn, imgPtrCol] = await db.imageTable();
  const retVal = await imgPtrCol.find({ phash }).limit(1).next();
  await conn.close();
  return retVal;
}

const storeImage = async (stats: ImageStats, imageBuffer: Buffer) => {
  const [conn, imgPtrCol] = await db.imageTable();

  const match = await imgPtrCol.find({ chash: stats.chash, phash: stats.phash }).toArray();
  if (match.length > 0) {
    await imgPtrCol.updateOne(
      { chash: stats.chash, phash: stats.phash },
      { $inc: { refCount: 1 } },
    );
  } else {
    await imgPtrCol.insertOne({
      chash: stats.chash,
      phash: stats.phash,
      image: imageBuffer,
      refCount: 1,
    });
  }
  await conn.close();
};

const statsFromFile = async (imageBuffer: Buffer): Promise<ImageStats | undefined> => {
  // Use python server to colorize image
  const formData = new FormData();
  // Base64 encode, otherwise the server may find a boundary inside of the image that is incorrect.
  // The server will also give us back a b64 encoded image.
  formData.append('image', imageBuffer.toString('base64'), 'image.jpeg');
  return (
    (
      await axios.post<ImageStats>('http://py-server:5001/stat', formData, {
        headers: { ...formData.getHeaders() },
      })
    ).data || undefined
  );
};

async function decrefOrDelete(imgPtrCol: mongodb.Collection, phash: string) {
  const update = { $inc: { refCount: -1 } };
  await imgPtrCol.updateOne({ phash }, update);
  imgPtrCol.deleteMany({ refCount: { $lte: 0 } });
}

async function linkedDelete(index: string) {
  const [userConn, userCol] = await db.getDB();
  const [imgPtrConn, imgPtrCol] = await db.imageTable();
  // console.log(await (await imgPtrCol.find({}).toArray()).map((h) => h.phash));
  const imageMetadata = await userCol.find({ index }).next();

  if (!imageMetadata) { return null; }

  await Promise.all([
    decrefOrDelete(imgPtrCol, imageMetadata.bw.phash),
    decrefOrDelete(imgPtrCol, imageMetadata.color.phash),
  ]);
  // console.log(await (await imgPtrCol.find({}).toArray()).map((h) => h.phash));
  const result = await userCol.deleteMany({ index: parseInt(index, 10) });
  await Promise.all([userConn.close(), imgPtrConn.close()]);
  return result;
}

const colorizeImage = async (bwBuffer: Buffer): Promise<[Buffer, ImageStats]> => {
  // Use python server to colorize image
  const formData = new FormData();
  // Base64 encode, otherwise the server may find a boundary inside of the image that is incorrect.
  // The server will also give us back a b64 encoded image.
  formData.append('image', bwBuffer.toString('base64'), 'image.jpeg');

  const colorizeRes = await axios.post<ColorizedImage>('http://py-server:5001/colorize/PIL', formData, {
    headers: { ...formData.getHeaders() },
  });

  // Decode the (colorized) image from base64.
  return [Buffer.from(colorizeRes.data.colorized, 'base64'), colorizeRes.data.stats];
};

export const getImagesList = async (req: any, res: BaseResponse) => {
  const metadata = await enumerateImageMetadata();
  res.contentType('application/json');
  res.send({ metadata });
  return res.end();
};

export const getImageData = async (req: IndexRequest, res: IndexResponse) => {
  const [userConn, userCol] = await db.getDB();
  userCol
    .find({ index: parseInt(req.params.imgidx, 10) })
    .limit(1)
    .next(async (err, doc) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.contentType('application/json');
        res.send(doc);
        res.end();
      }
      await userConn.close();
    });
};

export const patchImage = async (req: IndexRequest, res: IndexResponse) => {
  const [userConn, userCol] = await db.getDB();
  const [imgPtrConn, imgPtrCol] = await db.imageTable();
  const idx = req.params.imgidx;

  // Create a B&W copy of the uploaded image.
  const imageDocument = await userCol
    .find({ index: parseInt(idx, 10) })
    .limit(1)
    .next();
  if (!imageDocument) { res.sendStatus(404); } else {
    const bwBuffer = await imgPtrCol.find({ phash: imageDocument.bw.phash }).next();
    if (!bwBuffer) { res.sendStatus(404); } else {
      const [colorizedImage, colorizedStats] = await colorizeImage(bwBuffer.image);
      // Replace old color image by deleting old and storing new.
      await decrefOrDelete(imgPtrCol, imageDocument.color.phash);
      await storeImage(colorizedStats, colorizedImage);

      const filter = { index: imageDocument.index };
      const newValue = {
        $set: {
          color:
            { ...colorizedStats, url: template(colorizedStats.phash) },
        },
      };

      await userCol.updateOne(filter, newValue);
      res.sendStatus(200);
    }
  }
  await Promise.all([userConn.close(), imgPtrConn.close()]);
  return res.end();
};

export const postImage = async (req: UploadRequest, res: UploadResponse) => {
  const [userConn, userCol] = await db.getDB();
  const [imgPtrConn, imgPtrCol] = await db.imageTable();

  // Prevent server crash if image not present.
  if (!req.file) {
    res.sendStatus(415).end();
    return;
  }

  const fileData = fs.readFileSync(req.file.path);
  // Only allow images up to 7MB.
  // Mongodb supports up to 16MB docs, but we need to store 2 images per doc.
  // Therefore, 7MB is a safe limit, leaving room for other metadata.
  if (fileData.length > 7000000) {
    res.sendStatus(413).end();
    return;
  }

  // Create a B&W copy of the uploaded image.
  const jimpImage = await jimp.read(req.file.path);
  const bwBuffer = await jimpImage.greyscale().getBufferAsync(jimp.MIME_JPEG);
  // Get statistics about uploaded image (including hash).
  const bwStats = await statsFromFile(bwBuffer);
  if (!bwStats) {
    res.sendStatus(404).end();
    return;
  }

  // If image does not exist, insert it and return 200.
  // Otherwise, return 400, and don't re-insert image.
  if (await imgPtrCol.findOne({ phash: bwStats.phash })) {
    res.status(400).send(`${bwStats.phash} already exists.`);
  } else {
    const [colorizedBuffer, colorizedStats] = await colorizeImage(bwBuffer);

    // Get the maximum index, and increment it by one to get the new index. 0 if no items present.
    const pipeline = await userCol.aggregate([{ $group: { _id: null, max: { $max: '$index' } } }]).next();
    const newIndex = pipeline ? pipeline.max + 1 : 0;

    // Add an entry to the user's image table.
    // BW/color images should have the literal URL of the uploaded image.
    const doc = {
      index: newIndex,
      color: { ...colorizedStats, url: template(colorizedStats.phash) },
      bw: { ...bwStats, url: template(bwStats.phash) },
    };
    await userCol.insertOne(doc);
    // Insert both images into database.
    await Promise.all([storeImage(colorizedStats, colorizedBuffer), storeImage(bwStats, bwBuffer)]);

    res.send(doc);
  }
  await Promise.all([userConn.close(), imgPtrConn.close()]);
  res.end();
};

export const deleteAllImages = async (req: any, res: BaseResponse) => {
  const [conn, userCol] = await db.getDB();
  const [imgPtrConn, imgPtrCol] = await db.imageTable();
  const result = await userCol.deleteMany({});
  await imgPtrCol.deleteMany({});

  res.sendStatus(200);
  await conn.close();
  res.end();
};

export const deleteImageByHash = async (req: IndexRequest, res: IndexResponse) => {
  const [conn, userCol] = await db.getDB();

  const result = await linkedDelete(req.params.imgidx);

  if (result && result.deletedCount > 0) {
    res.sendStatus(200);
  } else {
    res.sendStatus(404);
  }

  await conn.close();
  return res.end();
};
