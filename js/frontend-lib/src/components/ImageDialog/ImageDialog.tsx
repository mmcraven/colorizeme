import * as React from 'react';
import './ImageDialog.scss';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import IconButton from '@material-ui/core/IconButton';
import useStyles from '../../hooks/useStyles';
import PhotoTile from '../PhotoTile';
import type { ImageDialogProps } from './ImageDialog.d';

const ImageDialog = (props: ImageDialogProps) => {
  const {
    selected, handleClose, handleRecolorize, userMetadata, incrementSelected,
  } = props;
  const classes = useStyles();
  const children = <div>
    <AppBar className={classes.titleBar}>
      <Toolbar>
        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
          <CloseIcon />
        </IconButton>
        <Button autoFocus color="inherit" onClick={handleRecolorize}>
          Recolorize
        </Button>
      </Toolbar>
    </AppBar>;

    {
      selected !== null && (
        <div style={{
          display: 'flex', justifyContent: 'center', alignItems: 'center',
        }}>
          <PhotoTile
            key={`dlg-${selected.index}`}
            metadata={userMetadata.find((item) => item.index === selected.index)}
            incrementSelected={incrementSelected}
          />
        </div>)
    }
  </div>;
  return <Dialog
    fullScreen
    open={selected !== null}
    onClose={handleClose}>
    <Slide direction="up" in={selected !== null} exit={selected === null}>
      {children}
    </Slide>
  </Dialog>;
};

export default ImageDialog;
