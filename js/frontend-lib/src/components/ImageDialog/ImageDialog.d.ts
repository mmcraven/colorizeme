import React from 'react';
import { ImageMetadata } from '../../types/images';

export interface ImageDialogProps {
  selected: ImageMetadata | null;
  handleClose: () => void;
  handleRecolorize: () => void;
  incrementSelected: (index: number) => void
  userMetadata: ImageMetadata[];
}
