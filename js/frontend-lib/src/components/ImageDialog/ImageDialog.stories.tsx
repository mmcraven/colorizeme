import React, { useState } from 'react';
import { ImageMetadata } from '../../types/images';
import ImageDialog from './ImageDialog';

export default {
  title: 'view/ImageDialog',
  component: ImageDialog,
  argTypes: {
  },
};

const Template = (args) => {
  const [index, setIndex] = useState(0);
  const images = args.urls.map((url) => ({ phash: 'null', chash: 'null', url }));
  const userMetadata = [{ selected: index, index: 0, images: Array(...images) }];
  const [selected, setSelected] = useState<ImageMetadata | null>(
    () => (args.open ? userMetadata[0] : null),
  );

  return <ImageDialog
    selected={selected}
    handleClose={() => { setSelected(null); }}
    handleRecolorize={() => undefined}
    incrementSelected={() => { setIndex((index + 1) % images.length); }}
    userMetadata={userMetadata} />;
};

export const closed = Template.bind({});
closed.args = {
  open: false,
  urls: ['https://imgs.xkcd.com/comics/password_strength.png'],
};

export const singleImage = Template.bind({});
singleImage.args = {
  open: true,
  urls: ['https://imgs.xkcd.com/comics/password_strength.png'],
};

export const doubleImage = Template.bind({});
doubleImage.args = {
  open: true,
  urls: ['https://imgs.xkcd.com/comics/open_source.png', 'https://imgs.xkcd.com/comics/pinouts.png'],
};
