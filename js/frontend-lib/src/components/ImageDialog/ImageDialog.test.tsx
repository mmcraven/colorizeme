import React from 'react';
import { shallow } from 'enzyme';
import ImageDialog from './ImageDialog';

describe('<ImageDialog />', () => {
  it('has been mounted', () => {
    const component = shallow(<ImageDialog
      selected={null}
      userMetadata={[]}
      handleClose={() => undefined}
      handleRecolorize={() => undefined}
      incrementSelected={() => undefined}
    />);
    expect(component.length).toBe(1);
  });
});
