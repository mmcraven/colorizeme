import React from 'react';

import { ImageMetadata } from '../../types/images';

export interface CustomTileProps {
  title: string;
  metadata: ImageMetadata;
  setSelected: (index: number) => void;
  incrementSelected: (index: number) => void;
}
