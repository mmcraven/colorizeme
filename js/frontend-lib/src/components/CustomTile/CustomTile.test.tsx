import React from 'react';
import { shallow } from 'enzyme';
import CustomTile from './CustomTile';

describe('<CustomTile />', () => {
  it('has been mounted', () => {
    const component = shallow(<CustomTile title="Placeholder"
      setSelected={() => undefined} incrementSelected={() => undefined}
      metadata={{ index: 1, images: [] }} />);
    expect(component.length).toBe(1);
  });
});
