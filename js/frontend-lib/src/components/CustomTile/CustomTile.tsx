import React from 'react';
import './CustomTile.scss';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import useStyles from '../../hooks/useStyles';
import PhotoTile from '../PhotoTile/PhotoTile';
import { CustomTileProps } from './CustomTile.d';

const CustomTile = (props: CustomTileProps) => {
  const {
    title, metadata, incrementSelected, setSelected,
  } = props;
  const classes = useStyles();
  return (
    <div className="CustomTile" data-testid="CustomTile">
      <PhotoTile metadata={metadata} incrementSelected={incrementSelected} />
      <div
        onClick={() => {
          // eslint-disable-next-line react/prop-types
          setSelected(metadata.index);
        }}>
        <GridListTileBar
          title={title}
          actionIcon={(
            <IconButton aria-label={`info about ${title}`} className={classes.icon}>
              <InfoIcon />
            </IconButton>
          )}
        />
      </div>
    </div >
  );
};

export default CustomTile;
