import React from 'react';
import './PhotoTile.scss';
import type { PhotoTileProps } from './PhotoTile.d';

const PhotoTile = (props: PhotoTileProps): React.ReactElement => {
  const { metadata, incrementSelected } = props;

  if (!metadata || metadata.images.length === 0) {
    return (<div
      className={'PhotoTile'}
      data-testid='PhotoTile'
    >No image to display :(</div>);
  }
  const selected = metadata.selected === undefined || metadata.selected > metadata.images.length
    ? undefined
    : metadata.images[metadata.selected];

  return (
    <div
      className={'PhotoTile'}
      data-testid='PhotoTile'
      onClick={() => {
        incrementSelected(metadata.index);
      }}
    >
      {selected && <img src={selected.url} alt={selected.phash} />}
      {!selected && <p>No available image</p>}
    </div>
  );
};

export default PhotoTile;
