import React, { useState } from 'react';
import PhotoTile from './PhotoTile';

export default {
  title: 'view/PhotoTile',
  component: PhotoTile,
  argTypes: {
  },
};

const Template = (args) => {
  const [index, setIndex] = useState(0);
  const images = args.urls.map((url) => ({ phash: 'null', chash: 'null', url }));
  return <PhotoTile
    incrementSelected={() => { setIndex((index + 1) % images.length); }}
    metadata={{ selected: index, index, images: Array(...images) }} />;
};

export const noImage = Template.bind({});
noImage.args = {
  title: 'Placeholder',
  urls: [],
};
export const singleImage = Template.bind({});
singleImage.args = {
  title: 'Placeholder',
  urls: ['https://imgs.xkcd.com/comics/password_strength.png'],
};

export const doubleImage = Template.bind({});
doubleImage.args = {
  title: 'Placeholder',
  urls: ['https://imgs.xkcd.com/comics/open_source.png', 'https://imgs.xkcd.com/comics/pinouts.png'],
};
