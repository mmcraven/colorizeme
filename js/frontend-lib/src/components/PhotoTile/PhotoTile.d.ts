import React from 'react';
import { ImageMetadata } from '../../types/images';

export interface PhotoTileProps {
  metadata?: ImageMetadata;
  incrementSelected: (index: number) => void;
}
