import React from 'react';
import { shallow } from 'enzyme';
import { RecoilRoot } from 'recoil';
import PhotoTile from './PhotoTile';

describe('<PhotoTile />', () => {
  it('has been mounted', () => {
    const metadata = {
      index: 0, title: '', url: '', images: [], selected: undefined,
    };
    // I need unused var for typing, supress linting.
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const incrementSelected = (_: number) => undefined;
    const component = shallow(
      <RecoilRoot>
        <PhotoTile metadata={metadata} incrementSelected={incrementSelected} />
      </RecoilRoot>,
    );
    expect(component.length).toBe(1);
  });
});
