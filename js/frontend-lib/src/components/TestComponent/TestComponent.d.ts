import React from 'react';

export interface TestComponentProps {
  heading: string;
  content: React.ReactNode;
}
