import React from 'react';
import TestComponent from './TestComponent';

export default {
  title: 'TestComponent',
  component: TestComponent,
  argTypes: {
  },
};

const Template = (args) => <TestComponent
  heading={args.heading}
  content={<h2>{args.content}</h2>}
/>;

export const WithText = Template.bind({});
WithText.args = {
  heading: 'Ahhhhh',
  content: 'I am texty text',
};

export const WithButtons = () => (
  <TestComponent
    heading="I have a button"
    content={
      <div>
        {/* eslint-disable-next-line no-alert */}
        <button onClick={() => alert('You clicked me!')}>Click me</button>
      </div>
    }
  />
);
