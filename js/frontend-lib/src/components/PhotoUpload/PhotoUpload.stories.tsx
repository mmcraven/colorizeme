import React from 'react';
import PhotoUpload from './PhotoUpload';

export default {
  title: 'view/PhotoUpload',
  component: PhotoUpload,
  argTypes: {
  },
};

const Template = () => <PhotoUpload />;

export const uploadImage = Template.bind({});
