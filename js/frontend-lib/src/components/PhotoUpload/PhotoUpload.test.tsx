import React from 'react';
import { shallow } from 'enzyme';
import PhotoUpload from './PhotoUpload';

describe('<PhotoUpload />', () => {
  it('has been mounted', () => {
    const component = shallow(<PhotoUpload />);
    expect(component.length).toBe(1);
  });
});
