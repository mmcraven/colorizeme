import React, { useEffect } from 'react';
import './PhotoUpload.scss';

const PhotoUpload = () => {
  const [imgPath, setImgPath] = React.useState<File | undefined>(undefined);
  const [preview, setPreview] = React.useState<string | undefined>(undefined);

  // Get a readable URL for the image path (if it exists)
  useEffect(() => {
    if (!imgPath) {
      setPreview(undefined);
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      return () => { };
    }
    const url = URL.createObjectURL(imgPath);
    setPreview(url);
    // Prevent memory leak.
    return () => URL.revokeObjectURL(url);
  }, [imgPath]);

  // Handle user selecting an image from the file dialog.
  const onSelectFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.files || event.target.files.length === 0) {
      setImgPath(undefined);
    } else {
      setImgPath(event.target.files[0]);
    }
  };

  // Handle click on "Upload" button.
  const onSubmit = async () => {
    if (!imgPath) return;

    // Put File into a form data, so that we can have multi-part data.
    const data = new FormData();
    data.append('image', imgPath);

    // Must set cors/multipart/form-data, or request will fail.
    fetch('http://localhost:5000/images', {
      method: 'POST',
      mode: 'cors',
      headers: {
        contentType: 'multipart/form-data',
      },
      body: data,
      // eslint-disable-next-line no-console
    }).then((res) => console.log(res));
  };
  return (
    <div className={'PhotoUpload'} data-testid="PhotoUpload" style={{ display: 'flex', flexDirection: 'column' }}>
      <label>
        Image:
        <input type="file" id="img" name="img" accept="image/*" onChange={onSelectFile} />
      </label>
      {/* Div to limit the maximum width of the preview */}
      <p />
      <div className={'LimitWidth'}>{imgPath && <img className={'Preview'} src={preview} alt="img" />}</div>
      <input type="submit" value="Upload" className={'Submit'} onClick={onSubmit} />
    </div>
  );
};

export default PhotoUpload;
