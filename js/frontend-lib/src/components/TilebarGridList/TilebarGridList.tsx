import React, { useEffect } from 'react';
import './TilebarGridList.scss';
import { useRecoilState } from 'recoil';
import { cloneDeep } from 'lodash';

import { ImageList, ImageListItem } from '@material-ui/core';
import userMetadataAtom from '../../atoms/imageAtoms';
import useStyles from '../../hooks/useStyles';
import CustomTile from '../CustomTile';
import ImageDialog from '../ImageDialog';

const TitlebarGridList = () => {
  const classes = useStyles();
  const [userMetadata, setUserMetadata] = useRecoilState(userMetadataAtom);
  // Dummy variable  we use to trigger a re-render when re-colorize is hit.
  const [colorIter, setColorIter] = React.useState(0);
  type indexMapType = { [key: number]: number }
  // For each image, track (index:active phash) pairs
  const [selectedIndicies, setSelectedIndicies] = React.useState<indexMapType>({});

  useEffect(() => {
    fetch('http://localhost:5000/images', {
      method: 'GET',
    })
      .then((res) => res.json())
      .then((result) => {
        const newIndicies: indexMapType = {};
        // Get the list of all hashes in the old set and not the new one.
        for (let index = 0; index < result.metadata.length; index += 1) {
          const item = result.metadata[index];
          // If we don't have a selected image for this image index, set it to the first image
          // eslint-disable-next-line no-param-reassign
          result.metadata[index].selected = selectedIndicies[item.index] || 0;
          // Then coallate the new indicies
          newIndicies[item.index] = item.selected;
        }

        // Update state variables.
        setUserMetadata(result.metadata);
        setSelectedIndicies(newIndicies);
      });
    // I'm ignoring selectedIndicies on purpose, because otherwise we get stuck in an infinite loop
    // This infinite loop is a mistake on React's part (object value doesn't change).
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [colorIter, setSelectedIndicies, setUserMetadata]);
  // console.log(userMetadata);

  const [selected, setSelected] = React.useState<number | null>(null);

  const handleClose = () => {
    setSelected(null);
  };

  // Cycle through the colorized / bw images.
  const incrementSelected = (index: number) => {
    // Prevent (expensive) clone if we aren't making changes
    if (userMetadata.find((item) => item.index === index) === undefined) return;
    // Don't modify state in place.
    const updatedMeta = cloneDeep(userMetadata);
    const toChange = updatedMeta.find((item) => item.index === index);
    if (!toChange || toChange.selected === undefined) return;
    // ++ selected index
    toChange.selected = toChange.images.length === 0
      ? undefined : (toChange.selected + 1) % toChange.images.length;
    setUserMetadata(updatedMeta);
    setSelectedIndicies({ ...selectedIndicies, [index]: toChange.selected as number });
  };

  // Helper to
  const handleRecolorize = () => {
    if (selected === null) return;
    // eslint-disable-next-line no-console
    console.log('Recoloring', selected);
    fetch(`http://localhost:5000/images/${selected}`, {
      method: 'PATCH',
    }).then(() => {
      // Update dummy variable to force re-render
      setColorIter(colorIter + 1);
    });
  };

  return (
    <div className={classes.root}>
      <ImageList
        cols={3} className={classes.gridList} component="li">
        {userMetadata.map((metadata) => (
          <ImageListItem key={metadata.index}>
            <CustomTile
              title={'Herr Lingus'}
              metadata={metadata}
              incrementSelected={incrementSelected}
              setSelected={setSelected}
            />
          </ImageListItem>
        ))}
      </ImageList>
      <ImageDialog selected={(userMetadata.find((item) => item.index === selected)) ?? null}
        handleClose={handleClose}
        handleRecolorize={handleRecolorize}
        incrementSelected={incrementSelected}
        userMetadata={userMetadata} />
    </div>
  );
};

export default TitlebarGridList;
