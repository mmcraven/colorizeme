import React from 'react';
import { shallow } from 'enzyme';
import { RecoilRoot } from 'recoil';
import TilebarGridList from './TilebarGridList';

describe('<TilebarGridList />', () => {
  it('has been mounted', () => {
    const component = shallow(<RecoilRoot><TilebarGridList /></RecoilRoot>);
    expect(component.length).toBe(1);
  });
});
