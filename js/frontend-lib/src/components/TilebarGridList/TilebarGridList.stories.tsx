import React from 'react';
import { RecoilRoot } from 'recoil';
import TilebarGridList from './TilebarGridList';

export default {
  title: 'TilebarGridList',
  component: TilebarGridList,
  argTypes: {
  },
};

const Template = () => <RecoilRoot><TilebarGridList /></RecoilRoot>;

export const withTemplate = Template.bind({});
