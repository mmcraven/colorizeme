import React from 'react';

export interface TilebarGridListProps {
  heading: string;
  content: React.ReactNode;
}
