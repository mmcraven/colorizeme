// In the future, I may choose a true "type" for hashes, but for now, it's just a string.
export type Hash = string;

export interface Image {
  phash: Hash;
  chash: Hash;
  url: string;
}
export interface ImageMetadata {
  index: number;
  images: Image[];
  selected?: number;
  // title?: string;
  // tags?: String[];
}
