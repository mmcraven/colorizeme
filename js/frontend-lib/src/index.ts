import TestComponent from './components/TestComponent';
import BodyAbout from './layout/BodyAbout/BodyAbout';
import BodyView from './layout/BodyView/BodyView';
import BodyUpload from './layout/BodyUpload/BodyUpload';

import Header from './layout/Header/Header';
import Footer from './layout/Footer/Footer';

export {
  BodyAbout, BodyView, BodyUpload, Header, Footer, TestComponent,
};
