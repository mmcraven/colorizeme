import { atom } from 'recoil';
import type { ImageMetadata } from '../types/images';
// An atom that tracks all current metadata for a given image set.
const userMetadataAtom = atom({ key: 'userMetadata', default: [] as Array<ImageMetadata> });
export default userMetadataAtom;
