import React from 'react';
import BodyView from './BodyView';

export default {
  title: 'layout/BodyView',
  component: BodyView,
  argTypes: {
  },
};

const Template = () => <BodyView />;

export const withTemplate = Template.bind({});
