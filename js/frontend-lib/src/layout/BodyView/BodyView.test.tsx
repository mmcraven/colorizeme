import React from 'react';
import { shallow } from 'enzyme';
import BodyView from './BodyView';

describe('<BodyView />', () => {
  it('has been mounted', () => {
    const component = shallow(<BodyView />);
    expect(component.length).toBe(1);
  });
});
