import React from 'react';
import './BodyView.scss';
import { RecoilRoot } from 'recoil';
import TitlebarGridList from '../../components/TilebarGridList';

const BodyView = () => (
  <RecoilRoot>
    <div className={'BodyView'} data-testid="BodyView">
      <h1>Material Image Grid</h1>
      <TitlebarGridList />
    </div>
  </RecoilRoot>
);

export default BodyView;
