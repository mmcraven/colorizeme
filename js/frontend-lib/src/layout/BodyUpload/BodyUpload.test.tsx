import React from 'react';
import { shallow } from 'enzyme';
import BodyUpload from './BodyUpload';

describe('<BodyUpload />', () => {
  it('has been mounted', () => {
    const component = shallow(<BodyUpload />);
    expect(component.length).toBe(1);
  });
});
