import React from 'react';
import BodyUpload from './BodyUpload';

export default {
  title: 'layout/BodyUpload',
  component: BodyUpload,
  argTypes: {
  },
};

const Template = () => <BodyUpload />;

export const withTemplate = Template.bind({});
