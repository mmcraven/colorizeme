import React from 'react';

import './BodyUpload.scss';
import PhotoUpload from '../../components/PhotoUpload/PhotoUpload';

const BodyUpload = () => (
  <div className={'BodyUpload'} data-testid="BodyUpload">
    <PhotoUpload />
  </div>
);

export default BodyUpload;
