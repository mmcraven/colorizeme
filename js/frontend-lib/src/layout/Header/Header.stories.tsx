import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './Header';

export default {
  title: 'layout/Header',
  component: Header,
  argTypes: {
  },
};

const Template = () => <Router><Header /></Router>;

export const withTemplate = Template.bind({});
