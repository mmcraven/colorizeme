import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('<Header />', () => {
  it('has been mounted', () => {
    const component = shallow(<Header />);
    expect(component.length).toBe(1);
  });
});
