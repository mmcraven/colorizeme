import {
  AppBar, Button, Toolbar, Menu, MenuItem,
} from '@material-ui/core';
import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import './Header.scss';

interface HeaderNavState {
  itemFlag: boolean;
}
const collectionItems = [
  { name: 'view', url: '/view' },
  { name: 'upload', url: '/upload' },
];
const Nav = styled.div``;
const Header = () => {
  const [state, setState] = useState<HeaderNavState>({ itemFlag: false });
  const [anchor, setAnchor] = useState<null | HTMLElement>(null);

  const menuOpened = (event: React.MouseEvent<HTMLButtonElement>, item: string): void => {
    setAnchor(event.currentTarget);
    switch (item) {
      case '1':
        setState({ ...state, itemFlag: true });
        break;
      default:
        setState({ ...state, itemFlag: false });
    }
  };

  const menuClosed = (): void => {
    setState({ ...state, itemFlag: false });
  };

  return (
    <Toolbar className={'Header'}>
      <AppBar>
        <Nav>
          {/* Button and content for collections */}
          <Button onClick={(e) => menuOpened(e, '1')}>Collections</Button>
          <Menu id="" anchorEl={anchor} open={state.itemFlag} onClose={menuClosed}>
            {collectionItems.map((item) => (
              <NavLink to={item.url} key={item.name}>
                <MenuItem onClick={() => menuClosed()}>{item.name}</MenuItem>
              </NavLink>
            ))}
          </Menu>

          {/* Button for about */}
          <NavLink to="/about" key="about">
            <Button>About</Button>
          </NavLink>
        </Nav>
      </AppBar>
    </Toolbar>
  );
};

export default Header;
