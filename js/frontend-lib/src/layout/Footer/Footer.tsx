import React from 'react';
import './Footer.scss';

const Footer = () => (
  <div className={'Footer'} data-testid="Footer">
    Magic Copyright
  </div>
);

export default Footer;
