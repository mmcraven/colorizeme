import React from 'react';
import { shallow } from 'enzyme';
import Footer from './Footer';

describe('<Footer />', () => {
  it('has been mounted', () => {
    const component = shallow(<Footer />);
    expect(component.length).toBe(1);
  });
});
