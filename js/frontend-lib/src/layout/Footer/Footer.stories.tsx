import React from 'react';
import Footer from './Footer';

export default {
  title: 'layout/Footer',
  component: Footer,
  argTypes: {
  },
};

const Template = () => <Footer />;

export const withTemplate = Template.bind({});
