import React from 'react';
import BodyAbout from './BodyAbout';

export default {
  title: 'layout/BodyAbout',
  component: BodyAbout,
  argTypes: {
  },
};

const Template = () => <BodyAbout />;

export const withTemplate = Template.bind({});
