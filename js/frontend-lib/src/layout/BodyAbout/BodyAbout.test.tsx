import React from 'react';
import { shallow } from 'enzyme';
import BodyAbout from './BodyAbout';

describe('<BodyAbout />', () => {
  it('has been mounted', () => {
    const component = shallow(<BodyAbout />);
    expect(component.length).toBe(1);
  });
});
