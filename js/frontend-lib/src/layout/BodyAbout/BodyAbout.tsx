import React from 'react';
import './BodyAbout.scss';

const BodyAbout = () => (
  <div className={'BodyAbout'} data-testid="BodyAbout">
    <h1>About</h1>
  </div>
);

export default BodyAbout;
