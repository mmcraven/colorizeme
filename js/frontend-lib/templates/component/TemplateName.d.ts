import React from 'react';

export interface TemplateNameProps {
  heading: string;
  content: React.ReactNode;
}
