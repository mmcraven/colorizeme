import React from 'react';
import './TemplateName.scss';

const TemplateName = () => <div className={'TemplateName'} data-testid="TemplateName"></div>;

export default TemplateName;
