// From: https://www.sitepen.com/blog/getting-started-with-electron-typescript-react-and-webpack
import { app, BrowserWindow } from 'electron';
import * as url from 'url';
import * as path from 'path';

function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true,
  }));
  win.webContents.openDevTools();
}

app.on('ready', createWindow);
