# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Adding new components
If you want to create a new componenet `npx generate-react-cli  component <componenet_name>` to create a normal componenet.
If you want a layout, `npx generate-react-cli  component <componenet_name> --type layout`.
See `generate-react-cli.json` for sample invocations.