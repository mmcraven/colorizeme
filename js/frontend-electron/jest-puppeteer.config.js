module.exports = {
  server: {
    command: 'npm run start',
    port: 3000,
    launchTimeout: 2000,
    debug: true,
  },
};
