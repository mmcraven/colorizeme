FROM node:16

WORKDIR /usr/src/
COPY ./ ./
WORKDIR /usr/src/backend
RUN npm install
ENV PATH=$PATH:/usr/src/backend/node_modules/node/bin;
CMD ["npm", "start"]