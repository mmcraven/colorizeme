import base64
import hashlib
from io import BytesIO
import json
import os
import random
import sys


import imagehash
from PIL import Image, ImageOps
from flask import Flask, request, send_file, jsonify
from flask.helpers import make_response
from flask.wrappers import Response

app = Flask(__name__)
app.secret_key = os.urandom(12)

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def serve_pil_image(pil_img):
    img_io = BytesIO()
    pil_img.save(img_io, 'JPEG')
    img_io.seek(0)
    data = img_io.getvalue()
    stats = stat_helper(data)
    data = base64.b64encode(data)
    response = make_response()
    response.content_type = 'application/json'
    # Safe to decode bytes, as it's all base64 chars.
    response.data = json.dumps({"colorized": data.decode("utf-8"), "stats": stats})

    return response


def colorize_image(buffer):
    file = Image.open(BytesIO(buffer)).convert('L')
    if random.randint(0, 1) == 0:
        return ImageOps.colorize(file, black="red", white="green").convert('RGB')
    else:
        return ImageOps.colorize(file, black="yellow", white="blue").convert('RGB')


@app.route('/helloworld')
def hello_world():
    return 'Hello World, with love from Python'


# Callback must return an HTTP response
def image_handler(request, callback):
    # check if the post request has the file part
    if 'image' not in request.files:
        return Response("Did not include image", status=400)
    file = request.files['image']
    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if file.filename == '':
        return Response("File must be named", status=400)
    elif allowed_file(file.filename):
        data = base64.b64decode(file.read())
        return callback(data)


def stat_helper(data: bytes):
    image = Image.open(BytesIO(data))
    content_hash = hashlib.sha256()
    content_hash.update(data)
    return {"phash": str(imagehash.phash(image)), "chash": content_hash.hexdigest(),
            "width": image.width, "height": image.height}


@ app.route('/stat', methods=['POST'])
def stat():
    def callback(data): return jsonify(stat_helper(data))
    return image_handler(request, callback)


@ app.route('/colorize/PIL', methods=['POST'])
def colorize_pil():
    def callback(data): return serve_pil_image(colorize_image(data))
    return image_handler(request, callback)
