from distutils.core import setup
from setuptools import find_packages
import pkg_resources
install_reqs = []
install_reqs.extend(["more-itertools", "overrides", "future", "matplotlib", "scipy", "Pillow"])
install_reqs.extend(["torch ~= 1.7.0", "torchvision ~= 0.8.1", "pytest ~= 6.1"])
setup(name='ColorizeMe',
      version='0.1.0',
      description='ML powered image colorization',
      author=['Matthew McRaven', "Connor Lu"],
      author_email='matthew.mcraven@gmail.com',
      url='https://github.com/Matthew-McRaven/colorizeme',
      install_reqs=install_reqs,
      python_requires='~= 3.9',
      packages=find_packages('src'),
      package_dir={'': 'src'},
)
